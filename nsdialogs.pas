(***********************************************************************************
 * LCLCarbon Sandboxing *                                                          *
 *                                                                                 *
 * Description:                                                                    *
 * TOpenDialog and TSaveDialog types do not use Powerbox used for sandboxing.      *
 * AppKit NSSavePanel and NSOpenPanel types use Powerbox.                          *
 * This unit patches the sandboxing impossibility by implementing TNSDialog types, *
 * designated to replace the TDialog types with the NSPanel types.                 *
 *                                                                                 *
 * Usage:                                                                          *
 * 1. Define carbon_sandboxing.                                                    *
 * 2. Remove the TDialog types, e.g.:                                              *
 * {$IFNDEF carbon_sandboxing}                                                     *
 *   OpenPictureDialog: TOpenPictureDialog;                                        *
 *   SaveDialog: TSaveDialog;                                                      *
 * {$IFDEF Base}                                                                   *
 *   OpenDialog: TOpenDialog;                                                      *
 * {$ENDIF}                                                                        *
 * {$ENDIF}                                                                        *
 * 3. Remove temporarily the TDialog objects from the lfm files.                   *
 * 4. Add nsdialogs.pas to the project.                                            *
 * 5. Add the NSDialogs unit in the implementation uses clause of all units, for   *
 * which the compiler reports errors for the above identifiers not found, e.g.:    *
 * uses GENERATOR                                                                  *
 * {$IFDEF carbon_sandboxing},                                                     *
 *      NSDialogs                                                                  *
 * {$ENDIF};                                                                       *
 *                                                                                 *
 * Developed by:                                                                   *
 * Copyright (C) 2012 COBATA Software, COBATA is trademark of Bozhidar Hinkov.     *
 *                                                                                 *
 * Date:                                                                           *
 * 06 March 2017                                                                   *
 *                                                                                 *
 * USE AND MODIFY THIS UNIT TO MEET YOUR NEEDS, BUT DO NOT CHANGE THIS HEADER!     *
 ***********************************************************************************)
unit NSDialogs;

{$MODE Delphi}

interface

uses
  SysUtils, Classes, AppKit, MacOSAll;

type
  TNSSaveDialog = class(NSSavePanel)
  public
    FileName: String;

    constructor Create; override;
    function Execute: Boolean; virtual;
  end;

  TNSOpenDialog = class(NSOpenPanel)
  public
    FileName: String;

    constructor Create; override;
    function Execute: Boolean; virtual;
  end;

  TNSOpenPictureDialog = class(TNSOpenDialog)
  public
    constructor Create; override;
  end;

var
  SaveDialog: TNSSaveDialog;
{$IFDEF Base}
  OpenDialog: TNSOpenDialog;
{$ENDIF}
  OpenPictureDialog: TNSOpenPictureDialog;

implementation

constructor TNSSaveDialog.Create;
begin
  savePanel;
  setTitle(CFSTRP('Save Dialog'));
  setDirectory(CFSTRP('WebProjects'));
  setRequiredFileType(CFSTRP('html'));
  setAllowedFileTypes(CFStringCreateArrayBySeparatingStrings(nil, CFSTRP('html,htm,txt'), CFSTRP(',')));
  setExtensionHidden(false);
end;

function TNSSaveDialog.Execute: Boolean;
var
  buffer: string[255];
  bufferPtr: StringPtr;
begin
  Result := Boolean(runModal);
  if Result then
  begin
    bufferPtr := @buffer;
    CFStringGetPascalString(NSSavePanel(self).filename, bufferPtr, 256, kCFStringEncodingUTF8);
    FileName := buffer;
  end;
end;

constructor TNSOpenDialog.Create;
begin
  openPanel;
  setTitle(CFSTRP('Open Dialog'));
  setDirectory(CFSTRP('WebProjects'));
  setRequiredFileType(CFSTRP('html'));
  setAllowedFileTypes(CFStringCreateArrayBySeparatingStrings(nil, CFSTRP('html,htm,txt'), CFSTRP(',')));
end;

function TNSOpenDialog.Execute: Boolean;
var
  buffer: string[255];
  bufferPtr: StringPtr;
begin
  Result := Boolean(runModal);
  if Result then
  begin
    bufferPtr := @buffer;
    CFStringGetPascalString(NSOpenPanel(self).filename, bufferPtr, 256, kCFStringEncodingUTF8);
    FileName := buffer;
  end;
end;

constructor TNSOpenPictureDialog.Create;
begin
  openPanel;
  setTitle(CFSTRP('Open Picture Dialog'));
  setDirectory(CFSTRP('WebProjects'));
  setAllowedFileTypes(CFStringCreateArrayBySeparatingStrings(nil, CFSTRP('png,gif,jpg,jpeg,bmp,ico,wmf,emf'), CFSTRP(',')));
end;

initialization
  SaveDialog := TNSSaveDialog.Create;
{$IFDEF Base}
  OpenDialog := TNSOpenDialog.Create;
{$ENDIF}
  OpenPictureDialog := TNSOpenPictureDialog.Create;

finalization
  //SaveDialog.FreeInstance;
{$IFDEF Base}
  //OpenDialog.FreeInstance;
{$ENDIF}
  //OpenPictureDialog.FreeInstance;

end.